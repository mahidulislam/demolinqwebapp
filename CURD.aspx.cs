﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using demoLinq.Models;

namespace demoLinq
{
    public partial class CURD : System.Web.UI.Page
    {
        Test_Emp_dbEntities db=new Test_Emp_dbEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                getdata();
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            Emp em = new Emp
            {
                EmpName = txtEmpName.Text,
                ContactNo = Int32.Parse(txtContact.Text)

            };

            db.Emps.Add(em);
            db.SaveChanges();
            getdata();
            reset();

        }

        public void getdata()
        {
            //var a = db.Emps.Select(m => new { EmployeeID = m.EmpId, EmployeeName = m.EmpName, ContactNo = m.ContactNo }).ToList();

            gvDisp.DataSource = db.Emps.ToList();
            gvDisp.DataBind();
        }

        public void reset()
        {
            txtEmpName.Text = "";
            txtContact.Text = "";
        }

        protected void OnRowEditing(object sender, GridViewEditEventArgs e)
        {
            gvDisp.EditIndex = e.NewEditIndex;
            this.getdata();
        }

        protected void OnRowUpdating(object sender, GridViewUpdateEventArgs e)
        {

            GridViewRow row = gvDisp.Rows[e.RowIndex];
            int eId = Convert.ToInt32(gvDisp.DataKeys[e.RowIndex].Values[0]);
            string name = (row.Cells[2].Controls[0] as TextBox).Text;
            string contact = (row.Cells[3].Controls[0] as TextBox).Text;


            Emp em = (from c in db.Emps where c.EmpId == eId select c).FirstOrDefault();
            em.EmpName = name;
            em.ContactNo = Int32.Parse(contact);
            db.SaveChanges();

            gvDisp.EditIndex = -1;
            this.getdata();
        }

        protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowIndex != gvDisp.EditIndex)
            {
                (e.Row.Cells[0].Controls[2] as LinkButton).Attributes["onclick"] = "return confirm('Do you want to delete this row?');";
            }
        }

        protected void OnRowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int eId = Convert.ToInt32(gvDisp.DataKeys[e.RowIndex].Values[0]);
            Emp em = (from c in db.Emps where c.EmpId == eId select c).FirstOrDefault();

            db.Emps.Remove(em);
            db.SaveChanges();
            
            this.getdata();
        }

        protected void gvDisp_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvDisp.EditIndex = -1;
            this.getdata();
        }

    }
}