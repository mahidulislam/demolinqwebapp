﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CURD.aspx.cs" Inherits="demoLinq.CURD" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            height: 61px;
            text-align: center;
            text-decoration: underline;
        }
        .auto-style3 {
            height: 126px;
        }
        .auto-style4 {
            height: 65px;
            text-align: center;
        }
        .auto-style5 {
            width: 314px;
            text-align: right;
        }
        .auto-style6 {
            width: 15px;
            text-align: center;
        }
        .auto-style7 {
            text-align: left;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table class="auto-style1">
                <tr>
                    <td class="auto-style2" style="vertical-align: top; background-color: #66FF99;"><strong>CURD OPERATION</strong></td>
                </tr>
                <tr>
                    <td class="auto-style3" style="vertical-align: top; background-color: #F66F99;">
                        <table class="auto-style1">
                            <tr>
                                <td class="auto-style5">&nbsp;</td>
                                <td class="auto-style6">&nbsp;</td>
                                <td class="auto-style7">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="auto-style5">NAME</td>
                                <td class="auto-style6">:</td>
                                <td class="auto-style7">
                                    <asp:TextBox ID="txtEmpName" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style5">CONTACT NUMBER</td>
                                <td class="auto-style6">:</td>
                                <td class="auto-style7">
                                    <asp:TextBox ID="txtContact" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style5">&nbsp;</td>
                                <td class="auto-style6">&nbsp;</td>
                                <td class="auto-style7">
                                    <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="Submit" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style4" style="vertical-align: top; background-color: #66FFF9;">
                        <asp:GridView ID="gvDisp" runat="server" DataKeyNames="EmpId" OnRowDataBound="OnRowDataBound" 
                            OnRowEditing="OnRowEditing" OnRowCancelingEdit="gvDisp_RowCancelingEdit" OnRowUpdating="OnRowUpdating"
                            OnRowDeleting="OnRowDeleting" EmptyDataText="No record found.!!!">
                            <Columns>
                                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: top; background-color: #66FF99;">&nbsp;</td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
